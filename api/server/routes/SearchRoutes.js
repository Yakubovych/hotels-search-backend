import Router from 'express';
import SearchController from '../controllers/SearchController';

const router = Router();

router.get('/', SearchController.getHotelsbySearchCriteria);
router.get('/tips', SearchController.getSearchTips);

export default router;
