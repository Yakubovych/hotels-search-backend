import database from "../src/models";
import Email from "../services/EmailsService";
import { WEB_UI_MAIL } from "../constants/config.env.json";
import { UPLOAD_COLLECTION_PATH } from "../constants/server.env.json";
import base64Img from "base64-img";

class AdminService {
  static async getListNotActiveHotels() {
    try {
      const notActiveHotels = await database.hotels.findAll({
        where: { status: "INACTIVE" },
        attributes: ["id", "name", "starRating"],
        include: [
          {
            model: database.addresses,
            attributes: ["city", "house", "street"]
          },
          { model: database.users, attributes: ["email", "fullname"] },
          {
            model: database.photo,
            attributes: ["photo_url"]
          },
          {
            model: database.rooms,
            attributes: ["id"],
            include: [
              { model: database.orders,
                attributes: ["id"]
              }
            ],
          }
        ]
      });

      notActiveHotels.map(async hotel => {
        hotel.photos.splice(1, hotel.photos.length - 1);
        if(hotel.photos.length > 0){
          hotel.photos[0].photo_url = await base64Img.base64Sync(
            `${UPLOAD_COLLECTION_PATH}${hotel.photos[0].photo_url}`
          );
        }
      });

      const countNotActiveHotels = await database.hotels.count({
        where: { status: "INACTIVE" }
      });

      return { notActiveHotels, countNotActiveHotels };
    } catch (e) {
      throw e;
    }
  }

  static async getListActiveHotels(status) {
    try {
      const listActiveHotels = await database.hotels.findAll({
        where: { status: "ACTIVATED" },
        attributes: ["id", "name", "starRating"],
        include: [
          {
            model: database.addresses,
            attributes: ["city", "house", "street"]
          },
          { model: database.users, attributes: ["email", "fullname"] },
          {
            model: database.photo,
            attributes: ["photo_url"]
          },
          {
            model: database.rooms,
            attributes: ["id"],
            include: [
              { model: database.orders,
                attributes: ["id"]
              }
            ],
          }
        ]
      });

      listActiveHotels.map(async hotel => {
        hotel.photos.splice(1, hotel.photos.length - 1);
        if(hotel.photos.length > 0){
          hotel.photos[0].photo_url = await base64Img.base64Sync(
            `${UPLOAD_COLLECTION_PATH}${hotel.photos[0].photo_url}`
          );
        }
      });

      const countActiveHotels = await database.hotels.count({
        where: { status: "ACTIVATED" }
      });

      return { listActiveHotels, countActiveHotels };
    } catch (e) {
      throw e;
    }
  }

  static async getListSuspendHotels() {
    try {
      const listSuspendHotels = await database.hotels.findAll({
        where: { status: "WAITINGFORAPPROVAL" },
        attributes: ["id", "name", "starRating"],
        include: [
          {
            model: database.addresses,
            attributes: ["city", "house", "street"]
          },
          { model: database.users, attributes: ["email", "fullname"] },
          {
            model: database.photo,
            attributes: ["photo_url"]
          },
          {
            model: database.rooms,
            attributes: ["id"],
            include: [
              { model: database.orders,
                attributes: ["id"]
              }
            ],
          }
        ]
      });

      listSuspendHotels.map(async hotel => {
        hotel.photos.splice(1, hotel.photos.length - 1);
        if(hotel.photos.length > 0){
          hotel.photos[0].photo_url = await base64Img.base64Sync(
            `${UPLOAD_COLLECTION_PATH}${hotel.photos[0].photo_url}`
          );
        }
      });


      const countSuspendHotels = await database.hotels.count({
        where: { status: "WAITINGFORAPPROVAL" }
      });

      return { listSuspendHotels, countSuspendHotels };
    } catch (e) {
      throw e;
    }
  }

  static async statusHotels(hotel, params) {
    try {
      return await database.hotels.update(
        { status: hotel.status },
        { where: { id: hotel.id } }
      );
    } catch (e) {
      throw e;
    }
  }

  static async sendEmailToOwner(email, message) {
    const TITLE = "Your hotel was suspended!";
    try {
      new Email.Builder(WEB_UI_MAIL)
        .setReceiver(email)
        .setHTML(message)
        .setSubject(TITLE)
        .build();
    } catch (e) {
      console.error(e);
    }
  }

  static async getCauntInactiveHotels() {
    try {
      const countInactiveHotels = await database.hotels.count({
        where: { status: "INACTIVE" }
      });
      return { countInactiveHotels };
    } catch (e) {
      console.error(e);
    }
  }
}

export default AdminService;
