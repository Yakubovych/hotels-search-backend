import SearchService from "../services/SearchService";
import { validateResponse, util } from "./ResponseWrapper";

const STATUS = require("../constants/status.code.env");

class SearchController {
  static async getHotelsbySearchCriteria(req, res) {
    try {
      const hotels = await SearchService.getHotelsbySearchCriteria(req, res);
      validateResponse(hotels.length, hotels, "Hotels by Criteria");
      return util.send(res);
    } catch (e) {
      util.setError(STATUS.BAD_REQUEST, e);
      return util.send(res);
    }
  }
 
    static async getSearchTips(req, res) {
      try {
        const hotels = await SearchService.getSearchTips(req, res);
        validateResponse(hotels.length, hotels, "Search Tips");
        return util.send(res);
      } catch (e) {
        util.setError(STATUS.BAD_REQUEST, e);
        return util.send(res);
      }
    }

}





export default SearchController;