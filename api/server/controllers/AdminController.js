import AdminService from "../services/AdminService";
import { util } from "./ResponseWrapper";
import * as STATUS from "../constants/status.code.env";
import MESSAGES from "../constants/messages.env";

class AdminController {
  static async getListNotActiveHotels(req, res) {
    try {
      let hotels = await AdminService.getListNotActiveHotels(); 
      util.setSuccess(STATUS.SUCCESS, MESSAGES.UTILS.SUCCESS, hotels);
      return util.send(res);
    } catch (error) {
      util.setError(STATUS.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getListActiveHotels(req,res) {
    try {
      const hotels = await AdminService.getListActiveHotels();
      util.setSuccess(STATUS.SUCCESS, MESSAGES.UTILS.SUCCESS, hotels);
      return util.send(res);
    } catch (error) {
      util.setError(STATUS.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getListSuspendHotels(req, res) {
    try {
      const hotels = await AdminService.getListSuspendHotels();
      util.setSuccess(STATUS.SUCCESS, MESSAGES.UTILS.SUCCESS, hotels);
      return util.send(res);
    } catch (error) {
      util.setError(STATUS.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async statusHotels(req, res) {
    try {
      const hotels = await AdminService.statusHotels(req.body, req.params);
      util.setSuccess(STATUS.SUCCESS, MESSAGES.UTILS.SUCCESS, hotels);
      return util.send(res);
    } catch (error) {
      util.setError(STATUS.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async sendEmailToOwner(req,res) {
    try {
      await AdminService.sendEmailToOwner(req.body.email, req.body.message );
      const email = req.body.email
      util.setSuccess(STATUS.SUCCESS, MESSAGES.EMAIL.SUCCESS, email);
      return util.send(res);
    }catch (error) {
      util.setError(STATUS.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getCauntInactiveHotels(req,res) {
    try {
      const count = await AdminService.getCauntInactiveHotels();
      util.setSuccess(STATUS.SUCCESS, MESSAGES.UTILS.SUCCESS, count);
      return util.send(res);
    } catch (error) {
      util.setError(STATUS.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default AdminController;
