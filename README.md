Requirements
For development, you will only need Node.js
$ node --version
v10.16.x

$ npm --version
6.10.x

Get Start
$ git clone https://gitlab.com/Yakubovych/hotels-search-backend.git
$ cd <folder_name>
$ npm i

Run project
$ npm start

Check result

Open http://localhost:8000 in your browser