import Router from 'express';
import HotelController from '../controllers/HotelController';
import * as VALIDATION  from '../constants/controller.env';
import {ID, PAGE, PARAM_PAGE, SEARCH} from '../constants/server.env.json';

const router = Router();

router.get(SEARCH,HotelController.validate(VALIDATION.GET_FEEDBACK_BY_HOTEL_ID), HotelController.getFeedbackbyHotelId);
router.get(`${PAGE}${PARAM_PAGE}`, HotelController.getAllHotels)
router.post('/', HotelController.validate(VALIDATION.ADD_HOTEL), HotelController.addHotel);
router.get(ID, HotelController.validate(VALIDATION.GET_HOTEL_BY_ID), HotelController.getHotelbyId);

export default router;

