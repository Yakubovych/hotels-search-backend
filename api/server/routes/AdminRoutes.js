import Router from "express";
import AdminController from "../controllers/AdminController";
import { HOTEL, HOTELS, ID, STATUS, INACTIVE, ACTIVE, SUSPEND, FORM, EMAIL, COUNT, OWNER } from "../constants/server.env";

const router = Router();

router.get(`${HOTELS}${INACTIVE}`, AdminController.getListNotActiveHotels);
router.put(`${STATUS}${HOTEL}${ID}`, AdminController.statusHotels);
router.get(`${HOTELS}${ACTIVE}`, AdminController.getListActiveHotels);
router.post(`${FORM}${EMAIL}`, AdminController.sendEmailToOwner);
router.get(`${HOTELS}${SUSPEND}`, AdminController.getListSuspendHotels);
router.get(`${HOTELS}${INACTIVE}${COUNT}`, AdminController.getCauntInactiveHotels);


export default router;