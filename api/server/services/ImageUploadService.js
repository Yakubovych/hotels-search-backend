import database from "../src/models";
class ImageUploadService {
  static async addImageProfile(image) {
    try {
      const result = await database.users.update(
        { profileImage: image.destination + "/" + image.filename },
        { where: { id: image.userId } }
      );
      return result;
    } catch (e) {
      throw e;
    }
  }

  static async addImageCollection(files, hotelId) {
    try {
      const result = files.map(async (file) => {
        const newRecord = new Object();
        newRecord.photo_url = `/hotel-${hotelId}-folder/${file}`;
        newRecord.hotel_id = hotelId;
        return await database.photo.create(newRecord);
      });
      return result;
    } catch (e) {
      throw e;
    }
  }

  static async getCollectionByHotelId(hotelId) {
    try {
      const result = await database.photo.findAll({
        where: { hotel_id: hotelId },
        attributes: [
          "photo_url"
        ],
      })
      return result;
    } catch (e) {
      throw e;
    }
  } 
}

export default ImageUploadService;
