import OwnerService from '../services/OwnerService';
import { validateResponse, util } from "./ResponseWrapper";
import { validationResult } from 'express-validator';
import { HOTEL_FIELDS } from '../constants/modelsFields';
import * as fieldValidator from '../validators/fieldValidator';
import * as STATUS from '../constants/status.code.env';
import { GET_HOTELS_BY_OWNER_ID }  from '../constants/controller.env';
import MESSAGES from '../constants/messages.env.json';

const { HOTELS_LIST: MESSAGE_HOTELS_LIST } = MESSAGES.HOTEL_CONTROLLER;

class OwnerController {
    static async getAllHotels(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                throw (errors.mapped());
            }
            const hotels = await OwnerService.getAllHotels(req.params.ownerId);
            validateResponse(hotels.length, hotels, MESSAGE_HOTELS_LIST);
            return util.send(res);
        } catch (error) {
            util.setError(STATUS.BAD_REQUEST, error);
            return util.send(res);
        }
    }

    static checkOwnerId() {
        return [
            ...fieldValidator.checkIntInParams(HOTEL_FIELDS.ownerId, { gt: 0 }),
        ];
    }

    static validate(method) {
        switch (method) {
            case GET_HOTELS_BY_OWNER_ID: return this.checkOwnerId();
        }
    }
}

export default OwnerController;