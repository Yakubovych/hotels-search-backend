import HotelService from '../services/HotelService';
import { validateResponse, util } from "./ResponseWrapper";
import { validationResult, check } from 'express-validator';
import * as fieldValidator from '../validators/fieldValidator';
import * as STATUS from "../constants/status.code.env";
import * as VALIDATION  from '../constants/controller.env';
import { HOTEL_FIELDS } from '../constants/modelsFields';
import AddressController from '../controllers/AddressController';
import AddressService from '../services/AddressService';
import MESSAGES from '../constants/messages.env.json';

const {
    HOTELS_LIST: MESSAGE_HOTELS_LIST,
    FOUND_HOTEL: MESSAGE_FOUND_HOTEL,
    HOTEL_ADDED: MESSAGE_HOTEL_ADDED,
    FOUND_FEEDBACK: MESSAGE_FOUND_FEEDBACK
} = MESSAGES.HOTEL_CONTROLLER;

class HotelController {
    static async getAllHotels(req, res) {
        try {
            const hotels = await HotelService.getAllHotels(req.params.page);
            validateResponse(hotels.length, hotels, MESSAGE_HOTELS_LIST);
            return util.send(res);
        } catch (error) {
            util.setError(STATUS.BAD_REQUEST, error);
            return util.send(res);
        }
    }

    static async getHotelbyId(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                throw (errors.mapped());
            }
            const hotel = await HotelService.getHotelbyId(req.params.id);
            validateResponse(!!hotel, hotel, MESSAGE_FOUND_HOTEL);
            return util.send(res);
        } catch (error) {
            util.setError(STATUS.BAD_REQUEST, error);
            return util.send(res);
        }
    }

    static async getFeedbackbyHotelId(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                throw (errors.mapped());
            }
            const feedbackSearch = await HotelService.getFeedbackbyHotelId(req.query.id,req.query.page,req.query.star);
            validateResponse(!!feedbackSearch, feedbackSearch, MESSAGE_FOUND_FEEDBACK);
            return util.send(res);
        } catch (error) {
            util.setError(STATUS.BAD_REQUEST, error);
            return util.send(res);
        }
    }



    static checkHotelFields() {
        return [
            ...AddressController.checkAddressFields(),
            ...fieldValidator.checkString(HOTEL_FIELDS.name, { min: 1, max: 70 }),
            ...fieldValidator.checkString(HOTEL_FIELDS.description, { min: 1, max: 1000 }),
            ...fieldValidator.checkInt(HOTEL_FIELDS.starRating, { gt: 0, lt: 6 }),
            ...fieldValidator.checkInt(HOTEL_FIELDS.ownerId, { gt: 0 }),
            check(HOTEL_FIELDS.ownerId).custom(fieldValidator.checkOwnerId)
        ];
    }

    static checkHotelId() {
        return [
            ...fieldValidator.checkIntInParams('id', { gt: 0 }),
        ];
    }

    static checkFeedbackByHotelId() {
        return [
            ...fieldValidator.checkInt('id', { gt: 0 }),
            ...fieldValidator.checkInt('page', { gt: 0 }),
        ];
    }

    static validate(method) {
        switch (method) {
            case VALIDATION.ADD_HOTEL: return this.checkHotelFields();
            case VALIDATION.GET_HOTEL_BY_ID: return this.checkHotelId();
            case VALIDATION.GET_FEEDBACK_BY_HOTEL_ID: return this.checkFeedbackByHotelId();
        }
    }

    static async addHotel(req, res) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                throw (errors.mapped());
            }
            const createdAddress = await AddressService.addAddress(req.body);
            req.body.addressId = createdAddress.dataValues.id;
            req.body.activated = false;
            const createdHotel = await HotelService.addHotel(req.body);
            util.setSuccess(STATUS.CREATED, MESSAGE_HOTEL_ADDED, createdHotel);
            return util.send(res);
        } catch (error) {
            util.setError(STATUS.BAD_REQUEST, error);
            return util.send(res);
        }
    }
}

export default HotelController;
