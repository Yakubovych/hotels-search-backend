import database from "../src/models";

class RoomService {
    static async getAllRooms(hotelId) {
        const rooms = await database.rooms.findAll({
            where: { hotelId: hotelId },
            attributes: ['id', 'price', 'roomNumber'],
            include: [
                { model: database.room_type },
                {
                    model: database.room_comforts,
                    attributes: ['id'],
                    include: [{ model: database.comforts }]
                }
            ]
        });
        return rooms;
    } catch (e) {
        throw e;
    }

    static async addRoom(room) {
        try {
        return await database.rooms.create(room);
        } catch (e) {
        throw e;
        }
    }
}

export default RoomService;
