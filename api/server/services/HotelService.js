import database from "../src/models";
import { UPLOAD_COLLECTION_PATH } from "../constants/server.env.json";
import {
  PAGINATION_LIMIT,
  FEEDBACK_PER_PAGE
} from "../constants/controller.env.json";
import base64Img from "base64-img";

const DEFAULT_FIND_ALL_ORDER = ['starRating', 'DESC'];
const Sequelize = require('sequelize');
const { sequelize } = database;

class HotelService {
  static async getAllHotels(page) {
    try {
      const hotelsList = await database.hotels.findAll({
        limit: PAGINATION_LIMIT,
        offset: page * PAGINATION_LIMIT - PAGINATION_LIMIT,
        where: { status: "ACTIVATED" },
        attributes: [
          "id",
          "name",
          "description",
          "starRating",
          "reviewsRating"
        ],
        order: [
          DEFAULT_FIND_ALL_ORDER
        ],
        include: [
          { model: database.addresses, attributes: ["city"] },
          {
            limit: PAGINATION_LIMIT,
            model: database.photo,
            attributes: ["photo_url"]
          }
        ]
      });
      const pageCount = await database.hotels.count() / PAGINATION_LIMIT;
      return [{ hotelsList: hotelsList, pageCount: Math.ceil(pageCount) }];
    } catch (e) {
      throw e;
    }
  }

  static async getHotelbyId(id) {
    try {
      const response = await database.hotels.findByPk(id, {
        include: [
          { model: database.addresses },
          { model: database.photo },
          {
            model: database.rooms,
            include: [

              { model: database.room_type },
              { model: database.room_comforts,
                    include: [{ model: database.comforts }]
              },
              { model: database.orders,
                include: [{model: database.feedbacks,}],
                limit: 1,
              }
            ],
          }
        ]
      });
          response.photos.map(async photo => photo.photo_url =
          await base64Img.base64Sync(`${UPLOAD_COLLECTION_PATH}${photo.photo_url}`));
      return response;
    } catch (e) {
      throw e;
    }
  }

  static async getFeedbackbyHotelId(id,page,star) {
    try {
      let whereStatement = `hotels.id=${id}`;
      if (star){whereStatement =`hotels.id=${id} and feedbacks.star=${star}`}
      const feedback = await sequelize.query(
          `SELECT feedbacks.id, feedbacks.star, feedbacks.message, feedbacks.created_at FROM hotels 
      JOIN rooms on hotels.id=rooms.hotel_id JOIN orders on orders.room_id=rooms.id 
      JOIN feedbacks on feedbacks.order_id=orders.id WHERE ${whereStatement}
      ORDER BY created_at DESC LIMIT ${FEEDBACK_PER_PAGE} OFFSET (${page} - 1) * ${FEEDBACK_PER_PAGE}`,
          { type:Sequelize.QueryTypes.SELECT});
      const feedbackCount = await sequelize.query(
          `SELECT COUNT(feedbacks.id) FROM hotels 
      JOIN rooms on hotels.id=rooms.hotel_id JOIN orders on orders.room_id=rooms.id 
      JOIN feedbacks on feedbacks.order_id=orders.id WHERE ${whereStatement}`,
          { type:Sequelize.QueryTypes.SELECT});
      return [{
        feedback: feedback,
        feedbackCount: feedbackCount,
      }];
    }
     catch (e) {
     console.log( e);
     throw e;
    }
  }

  static async addHotel(hotel) {
    try {
      return await database.hotels.create(hotel);
    } catch (e) {
      throw e;
    }
  }
}

export default HotelService;
