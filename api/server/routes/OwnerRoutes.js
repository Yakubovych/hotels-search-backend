import Router from "express";
import OwnerController from "../controllers/OwnerController";
import { OWNER_ID } from "../constants/server.env";
import { GET_HOTELS_BY_OWNER_ID }  from '../constants/controller.env';

const router = Router();

router.get(`${OWNER_ID}`, OwnerController.validate(GET_HOTELS_BY_OWNER_ID), OwnerController.getAllHotels);

export default router;